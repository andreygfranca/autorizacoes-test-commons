### Sobre o Projeto
Este projeto tem por objetivo de prover uma maneira programática para a criação de containers, bancos de dados e 
configurações para implementação de testes de integração.


### Utilização
Para escrever testes de integração é necessário anotar a classe de testes com `@IntegrationTest`, desta forma é possível
fazer a injeção de containers ou bancos de dados.

#### Instância de Banco de Dados do SGH
```java
@IntegrationTest
class TasyDatabaseSpec extends Specification {
    
    @InjectDatabase
    TasyDatabase database

    def "Should start a container running an Oracle Database for TASY"() {
        when:
        int testQuery = database.performQuery("SELECT 1 FROM DUAL").getInt(1)

        then:
        testQuery == 1
    }
  
}
```

#### Enviando e Recebendo mensagens Kafka

```groovy
@IntegrationTest
class SimpleKafkaTestITSpec extends Specification { 

  @Shared
  @InjectContainer
  ContainerInstance<KafkaContainer> kafkaContainer
  
  @Shared
  @Producer
  KafkaProducerClient<String> producer
  
  @Shared
  @Consumer
  KafkaConsumerClient consumer

  def "Should send kafka message"() {
    given:
    TestDTO teste = new TestDTO(foo: 5, bar: 1)

    when:
    producer.send("br.com.zg.teste", "1", teste)

    then:
    Awaitility.await().atMost(Duration.FIVE_SECONDS).untilAsserted {
      with(consumer.getSingleRecord(TestDTO, 'br.com.zg.teste')) { test ->
        test.foo == 5
        test.bar == 1
      }
    }
  }

}
```
