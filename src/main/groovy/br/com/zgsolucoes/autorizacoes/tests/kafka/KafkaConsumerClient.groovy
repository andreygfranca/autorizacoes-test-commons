package br.com.zgsolucoes.autorizacoes.tests.kafka

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.apache.kafka.clients.admin.AdminClient
import org.apache.kafka.clients.admin.NewTopic
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.clients.consumer.KafkaConsumer

import java.time.Duration
import java.util.stream.Collectors

class KafkaConsumerClient<T> {

  KafkaConsumer<String, byte[]> consumer
  AdminClient adminClient

  private static final ObjectMapper MAPPER = new ObjectMapper()
      .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
      .registerModule(new JavaTimeModule())
      .registerModule(new Jdk8Module())

  void configure(String bootstrapServer) {
    Properties properties = new Properties()
    properties.setProperty("bootstrap.servers", bootstrapServer)
    properties.setProperty("auto.create.topics.enable", "true")
    properties.setProperty("group.id", 'autorizacoes')
    properties.setProperty("client.id", UUID.randomUUID().toString())
    properties.setProperty("auto.offset.reset", "earliest")
    properties.setProperty("value.deserializer", "org.apache.kafka.common.serialization.ByteArrayDeserializer")
    properties.setProperty("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    properties.setProperty("enable.auto.commit", 'false')

    adminClient = getAdminClient(bootstrapServer)

    consumer = new KafkaConsumer<>(properties)
  }


  T getSingleRecord(Class<T> clazz, String topic) {
    consumer.subscribe([topic])
    List<T> records = getRecords(clazz, topic)
    consumer.unsubscribe()

    adminClient.deleteTopics([topic])
    adminClient.createTopics([new NewTopic(topic, [0: [0, 1, 2]])])

    return records != null && !records.empty ? records.last() : null
  }

  List<T> getRecords(Class<T> clazz, Duration duration = Duration.ofSeconds(5), String topic) {
    List<ConsumerRecord<String, byte[]>> fetched = []
    consumer.poll(duration).forEach { ConsumerRecord<String, byte[]> it -> fetched << it }
    List<ConsumerRecord<String, byte[]>> filtered = fetched.stream()
        .filter { it.topic() == topic }
        .collect(Collectors.toList())

    List records = filtered.stream().map {
      MAPPER.readValue(it.value(), clazz)
    }.collect(Collectors.toList())

    return records
  }

  void subscribe(Set<String> topic) {
    consumer.subscribe(topic)
  }

  AdminClient getAdminClient(String bootstrapServer) {
    Properties adminProps = new Properties()
    adminProps.setProperty('bootstrap.servers', bootstrapServer)
    adminClient = AdminClient.create(adminProps)
    return adminClient
  }
}
