package br.com.zgsolucoes.autorizacoes.tests.spock.processor

import br.com.zgsolucoes.autorizacoes.tests.kafka.KafkaProducerClient
import br.com.zgsolucoes.autorizacoes.tests.spock.annotation.Producer
import org.spockframework.runtime.extension.IMethodInterceptor
import org.spockframework.runtime.extension.IMethodInvocation

class KafkaProducerAnnotationProcessor implements IMethodInterceptor {

  @Override
  void intercept(IMethodInvocation invocation) throws Throwable {
    invocation.getSpec().getAllFields().findAll { it.isAnnotationPresent(Producer) }.forEach {
      Producer producerAnnotation = it.getAnnotation(Producer)

      KafkaProducerClient producerClient = new KafkaProducerClient()

      String bootstrapServer = producerAnnotation.bootstrapServer()
          ?: System.getProperty("br.com.zgsolucoes.kafka.bootstrap.server")

      producerClient.configure(bootstrapServer)

      it.writeValue(invocation.getInstance(), producerClient)
    }

    invocation.proceed()
  }

}
