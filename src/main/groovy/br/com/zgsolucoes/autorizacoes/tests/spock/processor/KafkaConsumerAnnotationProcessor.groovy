package br.com.zgsolucoes.autorizacoes.tests.spock.processor

import br.com.zgsolucoes.autorizacoes.tests.kafka.KafkaConsumerClient
import br.com.zgsolucoes.autorizacoes.tests.spock.annotation.Consumer
import org.spockframework.runtime.extension.IMethodInterceptor
import org.spockframework.runtime.extension.IMethodInvocation

class KafkaConsumerAnnotationProcessor implements IMethodInterceptor {

  @Override
  void intercept(IMethodInvocation invocation) throws Throwable {
    invocation.getSpec().getAllFields().findAll { it.isAnnotationPresent(Consumer) }.forEach {
      Consumer consumerAnnotation = it.getAnnotation(Consumer)

      KafkaConsumerClient consumerClient = new KafkaConsumerClient()

      String bootstrapServer = consumerAnnotation.bootstrapServer()
          ?: System.getProperty("br.com.zgsolucoes.kafka.bootstrap.server")

      consumerClient.configure(bootstrapServer)
      if (consumerAnnotation.topics()) {
        consumerClient.subscribe(consumerAnnotation.topics() as Set<String>)
      }

      it.writeValue(invocation.getInstance(), consumerClient)
    }

    invocation.proceed()
  }

}
