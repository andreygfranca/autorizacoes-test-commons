package br.com.zgsolucoes.autorizacoes.tests.container

import groovy.util.logging.Slf4j
import org.testcontainers.containers.PostgreSQLContainer

@Slf4j
@Singleton
class PostgresContainerInstance implements ContainerInstance<PostgreSQLContainer> {

  private static final POSTGRES_IMAGE = 'postgres:11.5-alpine'

  PostgreSQLContainer container

  @Override
  PostgreSQLContainer start() {
    log.debug("Starting postgres container ...")

    if (System.getProperty("br.com.zgsolucoes.postgres.started")) {
      log.warn "Postgres container already started."
      return null
    }

    container = new PostgreSQLContainer(POSTGRES_IMAGE)

    container
        .withReuse(true)
        .withNetwork(null)
        .start()

    System.setProperty("br.com.zgsolucoes.postgres.started", "true")

    log.debug("Postgres container successfully started")
    return container
  }

  @Override
  void close() {
    if (container?.isRunning()) {
      container.stop()
    }
  }
}
