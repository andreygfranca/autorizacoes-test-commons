package br.com.zgsolucoes.autorizacoes.tests.spock.processor

import br.com.zgsolucoes.autorizacoes.tests.container.ContainerFactory
import br.com.zgsolucoes.autorizacoes.tests.container.ContainerInstance
import br.com.zgsolucoes.autorizacoes.tests.spock.annotation.InjectContainer
import org.spockframework.runtime.extension.IMethodInterceptor
import org.spockframework.runtime.extension.IMethodInvocation

import java.lang.reflect.ParameterizedType

class InjectContainerAnnotationProcessor implements IMethodInterceptor {

  @Override
  void intercept(IMethodInvocation invocation) throws Throwable {
    invocation.getSpec().getAllFields().findAll { it.isAnnotationPresent(InjectContainer) }.forEach {
      InjectContainer injectContainer = it.getAnnotation(InjectContainer)
      
      Class<?> genericType = (it.reflection.genericType as ParameterizedType).actualTypeArguments[0] as Class<?>

      ContainerInstance container = ContainerFactory.create(genericType)

      if (injectContainer.autoStart()) {
        container.start()
      }

      it.writeValue(invocation.getInstance(), container)
    }

    invocation.proceed()
  }
}
