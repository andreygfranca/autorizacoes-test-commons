package br.com.zgsolucoes.autorizacoes.tests.database

import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException

interface WithDatabaseOperations {

  /**
   * Retrieve the connection with the database running in the container
   *
   * @return the jdbc connection with the database running inside a container
   */
  Connection getConnection()

  /**
   * Execute some specific sql query in the database
   *
   * @param sql the query to be executed
   *
   * @return
   */
  int performUpdate(String sql)

  /**
   * Execute a query in the database
   *
   * @param sql the query
   *
   * @return A ResultSet object that doesnt needs to
   *          move the cursor forward one row from its current position.
   *
   * @throws SQLException
   */
  ResultSet performQuery(String sql) throws SQLException

  /**
   * Retrieve the jdbc url of the database
   *
   * @return jdbc URL
   */
  String getJdbcUrl()

  /**
   * Retrieve the database user name
   * @return
   */
  String getUsername()

  /**
   * Retrieve the database user's password
   * @return
   */
  String getPassword()


}
