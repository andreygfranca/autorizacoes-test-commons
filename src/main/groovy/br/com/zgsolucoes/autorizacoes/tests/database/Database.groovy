package br.com.zgsolucoes.autorizacoes.tests.database

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.testcontainers.containers.JdbcDatabaseContainer
import org.testcontainers.spock.Testcontainers

import java.sql.*

/**
 * Represents an abstract database that could be any RDBMS (Mysql, Oracle, etc.)
 */
@Slf4j
@Testcontainers
@CompileStatic
abstract class Database<E extends JdbcDatabaseContainer> implements WithDatabaseOperations, WithScript {

  /**
   * The container that runs the RDBMS process.
   */
  protected E container

  @Override
  Connection getConnection() {
    return DriverManager.getConnection(
        container.getJdbcUrl(),
        container.getUsername(),
        container.getPassword()
    )
  }

  @Override
  int performUpdate(String sql) {
    Connection connection = getConnection()
    Statement stmt = connection.createStatement()
    return stmt.executeUpdate(sql)
  }

  @Override
  ResultSet performQuery(String sql) throws SQLException {
    Statement statement = getConnection().createStatement()
    statement.execute(sql)
    ResultSet resultSet = statement.getResultSet()

    resultSet.next()
    return resultSet
  }

  void executeScript(InputStream is) {
    String sql = is?.text
    if (!sql) {
      return
    }
    executeScript(sql)
  }

  void executeScript(String sql) {
    List<String> ddlCommands = splitSql(sql)
    if (ddlCommands?.isEmpty()) {
      return
    }

    for (String ddl in ddlCommands) {
      String adjustedDdl = ddl.replace("\r\n", " ").replace("\n", " ");
      log.debug("${adjustedDdl}")

      if (ddl.trim().size() != 0) {
        performUpdate(adjustedDdl)
      }
    }
  }

  List<String> splitSql(String sql) {
    sql?.startsWith("CREATE OR REPLACE PROCEDURE") ? [sql] : sql?.split("/") as List<String>
  }

  @Override
  String getJdbcUrl() {
    return container.getJdbcUrl()
  }

  @Override
  String getUsername() {
    return container.getUsername()
  }

  @Override
  String getPassword() {
    return container.getPassword()
  }

}
