package br.com.zgsolucoes.autorizacoes.tests.container

import org.testcontainers.containers.GenericContainer

/**
 * Representing a container instance acting as a delegator
 *
 * @param <T> Container type
 */
interface ContainerInstance<T extends GenericContainer> {

/**
 * Starts the container using docker, pulling an image if necessary.
 */
  T start()

  /**
   * Stops the container.
   */
  void close()

  /**
   * Retrieve the delegate container instance
   *
   * @return Container instance delegate
   */
  T getContainer()
}
