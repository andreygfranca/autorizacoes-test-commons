package br.com.zgsolucoes.autorizacoes.tests.spock.annotation

import br.com.zgsolucoes.autorizacoes.tests.database.ManagementSystem

import java.lang.annotation.*

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@interface InjectDatabase {

  /**
   * Database from specific management system to be instantiated {@link ManagementSystem}
   */
  ManagementSystem value() default ManagementSystem.TASY


  /**
   * Should start database automatically
   */
  boolean autoStart() default true

}
