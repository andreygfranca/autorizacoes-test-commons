package br.com.zgsolucoes.autorizacoes.tests.spock.processor

import br.com.zgsolucoes.autorizacoes.tests.database.Database
import br.com.zgsolucoes.autorizacoes.tests.database.DatabaseFactory
import br.com.zgsolucoes.autorizacoes.tests.spock.annotation.InjectDatabase
import org.spockframework.runtime.extension.IMethodInterceptor
import org.spockframework.runtime.extension.IMethodInvocation

class InjectDatabaseAnnotationProcessor implements IMethodInterceptor {

  @Override
  void intercept(IMethodInvocation invocation) throws Throwable {
    invocation.getSpec().getAllFields().findAll { it.isAnnotationPresent(InjectDatabase) }.forEach {
      InjectDatabase injectDatabase = it.getAnnotation(InjectDatabase)

      Database database = DatabaseFactory.createDatabase(injectDatabase.value())

      if (injectDatabase.autoStart()) {
        database.start()
      }

      it.writeValue(invocation.getInstance(), database)
    }

    invocation.proceed()
  }
}
