package br.com.zgsolucoes.autorizacoes.tests.spock.annotation

import java.lang.annotation.*

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@interface Consumer {

  String bootstrapServer() default ""

  String[] topics() default []

}
