package br.com.zgsolucoes.autorizacoes.tests.container

import org.testcontainers.containers.KafkaContainer
import org.testcontainers.containers.PostgreSQLContainer

class ContainerFactory {

  static ContainerInstance create(Class<?> containerInstanceGenericType) {

    switch (containerInstanceGenericType) {
      case KafkaContainer:
        return KafkaContainerInstance.instance

      case PostgreSQLContainer:
        return PostgresContainerInstance.instance

      default:
        return null
    }

  }
}
