package br.com.zgsolucoes.autorizacoes.tests.spock

import br.com.zgsolucoes.autorizacoes.tests.spock.annotation.IntegrationTest
import br.com.zgsolucoes.autorizacoes.tests.spock.processor.InjectContainerAnnotationProcessor
import br.com.zgsolucoes.autorizacoes.tests.spock.processor.InjectDatabaseAnnotationProcessor
import br.com.zgsolucoes.autorizacoes.tests.spock.processor.KafkaConsumerAnnotationProcessor
import br.com.zgsolucoes.autorizacoes.tests.spock.processor.KafkaProducerAnnotationProcessor
import groovy.util.logging.Slf4j
import org.spockframework.runtime.extension.AbstractAnnotationDrivenExtension
import org.spockframework.runtime.model.FieldInfo
import org.spockframework.runtime.model.SpecInfo

import java.lang.reflect.Modifier

@Slf4j
class IntegrationTestServiceFactoryExtension extends AbstractAnnotationDrivenExtension<IntegrationTest> {

  @Override
  void visitSpecAnnotation(IntegrationTest annotation, SpecInfo spec) {

    if (Modifier.isAbstract(spec.reflection.modifiers)) {
      return
    }

    spec.with {
      addSharedInitializerInterceptor(new InjectContainerAnnotationProcessor())
      addSharedInitializerInterceptor(new InjectDatabaseAnnotationProcessor())
      addSharedInitializerInterceptor(new KafkaProducerAnnotationProcessor())
      addSharedInitializerInterceptor(new KafkaConsumerAnnotationProcessor())
    }
  }

  @Override
  void visitFieldAnnotation(IntegrationTest annotation, FieldInfo field) {
    super.visitFieldAnnotation(annotation, field)
  }
}
