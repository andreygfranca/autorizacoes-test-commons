package br.com.zgsolucoes.autorizacoes.tests.spock.annotation

import br.com.zgsolucoes.autorizacoes.tests.spock.IntegrationTestServiceFactoryExtension
import org.spockframework.runtime.extension.ExtensionAnnotation

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@ExtensionAnnotation(IntegrationTestServiceFactoryExtension)
@interface IntegrationTest {

}
