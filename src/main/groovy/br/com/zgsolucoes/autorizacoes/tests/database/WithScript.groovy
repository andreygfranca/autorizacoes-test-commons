package br.com.zgsolucoes.autorizacoes.tests.database

/**
 * Provide an interface for executing an initial sql script.
 */
interface WithScript {

  /**
   * Executes a startup script after the container goes up
   * @param path is the location of the script
   */
  void executeScript(String path)

}
