package br.com.zgsolucoes.autorizacoes.tests.kafka

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerRecord

class KafkaProducerClient<K> {

  KafkaProducer<String, byte[]> producer

  private static final ObjectMapper MAPPER = new ObjectMapper()
      .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
      .registerModule(new JavaTimeModule())
      .registerModule(new Jdk8Module())

  void configure(String bootstrapServer) {
    Properties properties = new Properties()
    properties.setProperty("bootstrap.servers", bootstrapServer)
    properties.setProperty("auto.create.topics.enable", "true")
    properties.setProperty("group.id", 'autorizacoes')
    properties.setProperty("auto.offset.reset", "earliest")
    properties.setProperty("value.serializer", "org.apache.kafka.common.serialization.ByteArraySerializer")
    properties.setProperty("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    producer = new KafkaProducer<>(properties)
  }

  void send(String topic, K key, Object value) {
    byte[] convertedValue = MAPPER.writeValueAsBytes(value)
    ProducerRecord<K, byte[]> record = new ProducerRecord<>(
        topic,
        key,
        convertedValue,
    )
    producer.send(record)
  }
}
