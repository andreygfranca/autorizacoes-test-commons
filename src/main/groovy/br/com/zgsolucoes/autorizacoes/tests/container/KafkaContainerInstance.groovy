package br.com.zgsolucoes.autorizacoes.tests.container

import groovy.util.logging.Slf4j
import org.testcontainers.containers.KafkaContainer

@Slf4j
@Singleton
class KafkaContainerInstance implements ContainerInstance<KafkaContainer> {

  KafkaContainer container

  @Override
  KafkaContainer start() {
    log.debug("Starting kafka container...")
    if (System.getProperty("br.com.zgsolucoes.kafka.bootstrap.server")) {
      log.warn "Kafka container already started."
      return null
    }

    container = new KafkaContainer()

    container
        .withReuse(true)
        .withNetwork(null)
        .start()

    String bootstrapServer =
        "${container.getHost()}:${container.getMappedPort(9092)}"
    System.setProperty("br.com.zgsolucoes.kafka.bootstrap.server", bootstrapServer)

    log.debug("Kafka container successfully started at: $bootstrapServer.")
    return container
  }

  @Override
  void close() {
    if (container?.isRunning()) {
      container.stop()
    }
  }

}
