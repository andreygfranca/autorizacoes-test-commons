package br.com.zgsolucoes.autorizacoes.tests.database

import groovy.transform.CompileStatic

@CompileStatic
final class DatabaseFactory {

  static Database createDatabase(ManagementSystem managementSystem) {
    switch (managementSystem) {
      case ManagementSystem.TASY:
        return TasyDatabase.instance
      default:
        return null
    }
  }

}

