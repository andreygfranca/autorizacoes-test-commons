package br.com.zgsolucoes.autorizacoes.tests.database

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.testcontainers.containers.OracleContainer

@Slf4j
@Singleton
@CompileStatic
class TasyDatabase extends Database<OracleContainer> {

  /**
   * Define the image to be used to build the oracle container
   *
   * https://hub.docker.com/r/oracleinanutshell/oracle-xe-11g
   */
  private static final String ORACLE_IMAGE = "oracleinanutshell/oracle-xe-11g"

  void start() {
    log.debug("Starting TASY Database ...")

    if (System.getProperty("br.com.zgsolucoes.database.tasy.started")) {
      log.warn("TASY database already started")
      return
    }

    container = new OracleContainer(ORACLE_IMAGE)
        .withUsername('system')
        .withPassword('oracle')
        .withExposedPorts(1521, 7171)
        .withInitScript('grant.sql')
        .withReuse(true)

    container.start()
    executeScript(Thread.currentThread().getContextClassLoader().getResourceAsStream("tasy/schema.sql"))

    System.setProperty("br.com.zgsolucoes.database.tasy.started", "true")

    log.debug("TASY Database successfully started")
  }

  void close() {
    container.stop()
  }

}

