package br.com.zgsolucoes.autorizacoes.tests.spock.annotation

import java.lang.annotation.*

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@interface Producer {

  String bootstrapServer() default ""

  String[] properties() default [];

}
