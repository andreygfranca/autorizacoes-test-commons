CREATE USER TASY IDENTIFIED BY oracle;
GRANT CONNECT,RESOURCE,DBA TO TASY;
GRANT CREATE SESSION, GRANT ANY PRIVILEGE TO TASY;
GRANT UNLIMITED TABLESPACE TO TASY;
ALTER SESSION SET CURRENT_SCHEMA = TASY;
