SELECT
    aut.nr_sequencia                                                                          id,

    aut.nr_atendimento                                                                        id_atendimento,
    COALESCE(pos_atend.idx - 1, 0)                                                            idx_atendimento,

    prest_pj.nm_fantasia                                                                      nome_prestador_interno,
    prest.cd_estabelecimento                                                                  id_prestador_interno,

    convenio_pj.cd_ans                                                                        ans_operadora,
    convenio.ds_convenio                                                                      nome_operadora_interno,
    convenio.cd_convenio                                                                      id_operadora_interno,

    aut_estagio.nr_sequencia                                                                  cd_interno_status_autorizacao,
    aut_estagio.ds_estagio                                                                    status_autorizacao,

    aut.cd_senha                                                                              senha,
    aut.cd_autorizacao                                                                        numero_guia_operadora,
    aut.dt_validade_guia                                                                      validade_senha,
    aut.dt_autorizacao                                                                        data_inclusao,
    agendamento.dt_agendamento                                                                data_agendamento,
    atend.dt_entrada                                                                          data_atendimento,
    (
        SELECT max(hist.dt_atualizacao)
        FROM tasy.autorizacao_convenio_hist hist
        WHERE
            hist.nr_sequencia_autor = aut.nr_sequencia
            AND hist.nr_seq_estagio IN (64) -- TODO: Passar lista dos estagios mapeados para "SOLICITANDO" por parametro
    )                                                                                         data_solicitacao,
    (
        SELECT max(hist.dt_atualizacao)
        FROM tasy.autorizacao_convenio_hist hist
        WHERE
            hist.nr_sequencia_autor = aut.nr_sequencia
            AND hist.nr_seq_estagio IN (58) -- TODO: Passar lista dos estagios mapeados para "AUTORIZADO" por parametro
    )                                                                                         data_autorizacao,
    aut.ds_observacao                                                                         observacao,
    'SOLICITACAO_PREVIA'                                                                      origem_autorizacao,
    aut.ie_carater_int_tiss                                                                   carater_atendimento_interno,
    aut.ie_tipo_autorizacao                                                                   tipo_autorizacao_interno,
    aut.ie_tipo_guia                                                                          tipo_guia_interno,
    -- TODO: Tornar parametrizavel
    CASE
        WHEN nvl(procedencia_atendimento.nr_seq_grupo, procedencia_agendamento.nr_seq_grupo) = 1 THEN 1
        WHEN nvl(procedencia_atendimento.nr_seq_grupo, procedencia_agendamento.nr_seq_grupo) = 2 THEN 3
        ELSE nvl(atend.ie_tipo_atendimento, agendamento.ie_tipo_atendimento)
    END                                                                                       tipo_atendimento_interno,
    aut.ds_indicacao                                                                          indicacao_clinica,
    atend_categoria.nr_doc_conv_principal                                                     numero_guia_principal,

    beneficiario_pf.nm_pessoa_fisica                                                          nome_benefeciario,
    beneficiario_pf.nr_ddd_celular || beneficiario_pf.nr_telefone_celular                     telefone_beneficiario,
    0                                                                                         beneficiario_recem_nato,
    tasy.obter_dados_autor_convenio(aut.nr_sequencia, 'CU')                                   carteria_beneficiario,


    nvl(medico_solicitante_pf.nm_pessoa_fisica, medico_solicitante_atend_pf.nm_pessoa_fisica) nm_prof_solicitante,
    nvl(medico_solicitante.nr_crm, medico_solicitante_atend.nr_crm)                           cd_conselho_prof_solicitante,
    'CRM'                                                                                     sg_conselho_prof_solicitante,
    nvl(medico_solicitante.uf_crm, medico_solicitante_atend.uf_crm)                           uf_conselho_prof_solicitante,
    nvl(medico_solicitante_cbo.cd_cbo, medico_solicitante_atend_cbo.cd_cbo)                   cd_cbos_prof_solicitante,

    medico_executante_pf.nm_pessoa_fisica                                                     nm_prof_executante,
    medico_executante.nr_crm                                                                  cd_conselho_prof_executante,
    'CRM'                                                                                     sg_conselho_prof_executante,
    medico_executante.uf_crm                                                                  uf_conselho_prof_executante,
    medico_executante_cbo.cd_cbo                                                              cd_cbos_prof_executante,
    setor.cd_setor_atendimento                                                                cd_setor,
    setor.ds_descricao                                                                        ds_setor,

    (
        SELECT
            ('[' || LISTAGG(
                    '{"codigo":"' || proc.cd_procedimento || '"'
                            || ',"descricaoHex":"' || RAWTOHEX(utl_raw.cast_to_raw(CONVERT(proc.ds_procedimento, 'utf8'))) || '"'
                            || ',"quantidade":' || proc_aut.qt_solicitada
                            || '}',
                    ',') WITHIN GROUP (ORDER BY proc.cd_procedimento)
                    || ']'
                ) arr
        FROM
            tasy.procedimento_autorizado proc_aut
                JOIN tasy.procedimento proc
                     ON proc.cd_procedimento = proc_aut.cd_procedimento
                             AND proc.ie_origem_proced = proc_aut.ie_origem_proced
        WHERE proc_aut.nr_sequencia_autor = aut.nr_sequencia
    )                                                                                         arr_procedimentos,

    NULL                                                                                      arr_diagnostico,

    CASE
        WHEN exists(SELECT 1 FROM tasy.anexo_agenda aa WHERE aa.nr_seq_agenda = aut.nr_seq_agenda)
            THEN (
                     SELECT
                         (LISTAGG(
                                 'anexo_agenda/' || aa.nr_sequencia || ':'
                                         || REGEXP_REPLACE(
                                         utl_raw.cast_to_varchar2(
                                                 utl_encode.base64_encode(utl_raw.cast_to_raw(CONVERT(aa.ds_arquivo, 'utf8')))),
                                         '[[:space:]]', '')
                             , ';') WITHIN GROUP (ORDER BY aa.nr_seq_agenda)) anexos
                     FROM tasy.anexo_agenda aa
                     WHERE
                         aa.ds_arquivo IS NOT NULL
                         AND aa.nr_seq_agenda = aut.nr_seq_agenda
        )
        ELSE
            (
                SELECT
                    (LISTAGG(
                            'ged_atendimento/' || ga.nr_sequencia || ':'
                                    || REGEXP_REPLACE(
                                    utl_raw.cast_to_varchar2(utl_encode.base64_encode(utl_raw.cast_to_raw(CONVERT(ga.ds_arquivo, 'utf8')))),
                                    '[[:space:]]', '')
                        , ';') WITHIN GROUP (ORDER BY ga.nr_seq_agenda)) anexos
                FROM tasy.ged_atendimento ga
                WHERE
                    ga.nr_atendimento = atend.nr_atendimento
                    AND ga.ds_arquivo IS NOT NULL
                    AND ga.nr_seq_tipo_arquivo = 2 -- Pedido Medico
                    AND ga.ie_situacao = 'A' -- Ativo
        )
    END                                                                                       arr_anexos
FROM
    tasy.autorizacao_convenio aut
        JOIN tasy.estagio_autorizacao aut_estagio
             ON aut.nr_seq_estagio = aut_estagio.nr_sequencia

        LEFT JOIN tasy.agenda_paciente agendamento
                  ON aut.nr_seq_agenda = agendamento.nr_sequencia
        LEFT JOIN tasy.setor_atendimento setor
                  ON nvl(agendamento.cd_setor_atendimento, aut.cd_setor_origem) = setor.cd_setor_atendimento
        LEFT JOIN tasy.procedencia procedencia_agendamento
                  ON agendamento.cd_procedencia = procedencia_agendamento.cd_procedencia

        LEFT JOIN (
                      SELECT nr_sequencia, ROW_NUMBER() OVER (PARTITION BY ac_aux.nr_atendimento ORDER BY ac_aux.dt_autorizacao) idx
                      FROM tasy.autorizacao_convenio ac_aux
                      WHERE ac_aux.nr_atendimento IS NOT NULL
                  ) pos_atend
                  ON pos_atend.nr_sequencia = aut.nr_sequencia

        JOIN tasy.estabelecimento prest
             ON aut.cd_estabelecimento = prest.cd_estabelecimento
        JOIN tasy.pessoa_juridica prest_pj
             ON prest.cd_cgc = prest_pj.cd_cgc

        LEFT JOIN tasy.pessoa_fisica beneficiario_pf
                  ON aut.cd_pessoa_fisica = beneficiario_pf.cd_pessoa_fisica

        LEFT JOIN tasy.atendimento_paciente atend
                  ON atend.nr_atendimento = aut.nr_atendimento
        LEFT JOIN tasy.atend_categoria_convenio atend_categoria
                  ON atend_categoria.nr_atendimento = atend.nr_atendimento
        LEFT JOIN tasy.procedencia procedencia_atendimento
                  ON atend.cd_procedencia = procedencia_atendimento.cd_procedencia

        LEFT JOIN tasy.medico medico_solicitante
                  ON aut.cd_medico_solicitante = medico_solicitante.cd_pessoa_fisica
        LEFT JOIN tasy.pessoa_fisica medico_solicitante_pf
                  ON aut.cd_medico_solicitante = medico_solicitante_pf.cd_pessoa_fisica
        LEFT JOIN tasy.cbo_saude medico_solicitante_cbo
                  ON medico_solicitante_pf.nr_seq_cbo_saude = medico_solicitante_cbo.nr_sequencia

        LEFT JOIN tasy.medico medico_solicitante_atend
                  ON atend.cd_medico_resp = medico_solicitante_atend.cd_pessoa_fisica
        LEFT JOIN tasy.pessoa_fisica medico_solicitante_atend_pf
                  ON atend.cd_medico_resp = medico_solicitante_atend_pf.cd_pessoa_fisica
        LEFT JOIN tasy.cbo_saude medico_solicitante_atend_cbo
                  ON medico_solicitante_atend_pf.nr_seq_cbo_saude = medico_solicitante_atend_cbo.nr_sequencia

        LEFT JOIN tasy.medico medico_executante
                  ON aut.cd_medico_exec_agenda = medico_executante.cd_pessoa_fisica
        LEFT JOIN tasy.pessoa_fisica medico_executante_pf
                  ON aut.cd_medico_exec_agenda = medico_executante_pf.cd_pessoa_fisica
        LEFT JOIN tasy.cbo_saude medico_executante_cbo
                  ON medico_executante_pf.nr_seq_cbo_saude = medico_executante_cbo.nr_sequencia

        JOIN tasy.convenio convenio
             ON aut.cd_convenio = convenio.cd_convenio
        JOIN tasy.pessoa_juridica convenio_pj
             ON convenio_pj.cd_cgc = convenio.cd_cgc
WHERE
    -- TODO: Tornar setores parametrizaveis
    nvl(agendamento.cd_setor_atendimento, aut.cd_setor_origem) IN ('174', '175', '176', '177', '178', '179', '180', '181', '182', '192',
                                                                   '216', '233', '234', '191', '172', '173', '190', '163', '165', '167',
                                                                   '168', '153', '141', '160', '161')
--     AND aut.dt_autorizacao <= :dataInicio
--     AND (:tiposAutorizacoes IS NULL OR aut.ie_tipo_autorizacao IN (:tiposAutorizacoes))
--     AND convenio.cd_convenio IN (:chavesIntegracaoConvenio)
--     AND prest.cd_estabelecimento IN (:identificadoresInternoPrestador)
ORDER BY
    nvl(agendamento.dt_agenda, atend.dt_entrada)
