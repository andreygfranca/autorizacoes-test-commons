package br.com.zgsolucoes.autorizacoes.tests

import br.com.zgsolucoes.autorizacoes.tests.container.ContainerInstance
import br.com.zgsolucoes.autorizacoes.tests.spock.annotation.InjectContainer
import br.com.zgsolucoes.autorizacoes.tests.spock.annotation.IntegrationTest
import org.testcontainers.containers.KafkaContainer
import spock.lang.Shared
import spock.lang.Specification

@IntegrationTest
abstract class SuperClassIntegrationSpec extends Specification {

  @Shared
  @InjectContainer
  ContainerInstance<KafkaContainer> kafkaInstance

  @Shared
  String host

  void setupSpec() {
    host = kafkaInstance.container.bootstrapServers
  }

}
