package br.com.zgsolucoes.autorizacoes.tests.kafka

import br.com.zgsolucoes.autorizacoes.tests.SuperClassIntegrationSpec
import br.com.zgsolucoes.autorizacoes.tests.spock.annotation.Consumer
import br.com.zgsolucoes.autorizacoes.tests.spock.annotation.IntegrationTest
import br.com.zgsolucoes.autorizacoes.tests.spock.annotation.Producer
import org.awaitility.Awaitility
import org.awaitility.Duration
import spock.lang.Shared

@IntegrationTest
class KafkaProducerClient2Spec extends SuperClassIntegrationSpec {

  @Shared
  @Producer
  KafkaProducerClient<String> producer

  @Shared
  @Consumer
  KafkaConsumerClient consumer

  def "Should send kafka message"() {
    given:
    TestDTO teste = new TestDTO(foo: 3, bar: 3)

    when:
    producer.send("br.com.zg.teste", "1", teste)

    then:
    Awaitility.await().atMost(Duration.TEN_SECONDS).untilAsserted {
      with(consumer.getSingleRecord(TestDTO, 'br.com.zg.teste')) { TestDTO test ->
        test.foo == 3
        test.bar == 3
      }
    }
  }

  def "Should send kafka message2"() {
    given:
    TestDTO teste = new TestDTO(foo: 10, bar: 10)

    when:
    producer.send("br.com.zg.teste2", "1", teste)

    then:
    Awaitility.await().atMost(Duration.TEN_SECONDS).untilAsserted {
      with(consumer.getSingleRecord(TestDTO, 'br.com.zg.teste2'), TestDTO) {
        it.foo == 10
        it.bar == 10
      }
    }
  }

  static class TestDTO {
    int foo
    int bar
  }
}
