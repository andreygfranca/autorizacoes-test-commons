package br.com.zgsolucoes.autorizacoes.tests.kafka

import br.com.zgsolucoes.autorizacoes.tests.container.ContainerInstance
import br.com.zgsolucoes.autorizacoes.tests.spock.annotation.InjectContainer
import br.com.zgsolucoes.autorizacoes.tests.spock.annotation.IntegrationTest
import org.testcontainers.containers.KafkaContainer
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification

@IntegrationTest
class InjectKafkaContainerSpec extends Specification {

  @Shared
  @AutoCleanup
  @InjectContainer
  ContainerInstance<KafkaContainer> kafkaContainer

  @Shared
  @AutoCleanup
  @InjectContainer(autoStart = false)
  ContainerInstance<KafkaContainer> kafkaContainerNotStarted

  def "Should instantiate Kafka Container when inject"() {
    given:
    kafkaContainer
    expect:
    kafkaContainer.container != null
  }

  def "Should not start container with autoStart disabled"() {
    given:
    kafkaContainerNotStarted
    expect:
    !kafkaContainerNotStarted.container
  }
}
