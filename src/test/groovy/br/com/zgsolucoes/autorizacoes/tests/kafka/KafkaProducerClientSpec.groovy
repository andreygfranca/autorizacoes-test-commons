package br.com.zgsolucoes.autorizacoes.tests.kafka

import br.com.zgsolucoes.autorizacoes.tests.SuperClassIntegrationSpec
import br.com.zgsolucoes.autorizacoes.tests.spock.annotation.Consumer
import br.com.zgsolucoes.autorizacoes.tests.spock.annotation.IntegrationTest
import br.com.zgsolucoes.autorizacoes.tests.spock.annotation.Producer
import org.awaitility.Awaitility
import org.awaitility.Duration
import spock.lang.Shared

@IntegrationTest
class KafkaProducerClientSpec extends SuperClassIntegrationSpec {

  @Shared
  @Producer
  KafkaProducerClient<String> producer

  @Shared
  @Consumer
  KafkaConsumerClient consumer

  def "Should send kafka message"() {
    given:
    TestDTO teste = new TestDTO(foo: 5, bar: 1)

    when:
    producer.send("br.com.zg.teste", "1", teste)

    then:
    Awaitility.await().atMost(Duration.TEN_SECONDS).untilAsserted {
      with(consumer.getSingleRecord(TestDTO, 'br.com.zg.teste')) { TestDTO test ->
        test.foo == 5
        test.bar == 1
      }
    }
  }

  def "Should send kafka message2"() {
    given:
    TestDTO teste = new TestDTO(foo: 15, bar: 15)

    when:
    producer.send("br.com.zg.teste2", "1", teste)

    then:
    Awaitility.await().atMost(Duration.TEN_SECONDS).untilAsserted {
      with(consumer.getSingleRecord(TestDTO, 'br.com.zg.teste2'), TestDTO) {
        it.foo == 15
        it.bar == 15
      }
    }
  }

  def "Should send kafka message3"() {
    given:
    TestDTO teste = new TestDTO(foo: 0, bar: 0)

    when:
    producer.send("br.com.zg.teste3", "1", teste)

    then:
    Awaitility.await().atMost(Duration.TEN_SECONDS).untilAsserted {
      with(consumer.getSingleRecord(TestDTO, 'br.com.zg.teste3'), TestDTO) {
        it.foo == 0
        it.bar == 0
      }
    }
  }

  static class TestDTO {
    int foo
    int bar
  }
}
