package br.com.zgsolucoes.autorizacoes.tests.spock

import br.com.zgsolucoes.autorizacoes.tests.SuperClassIntegrationSpec
import br.com.zgsolucoes.autorizacoes.tests.spock.annotation.IntegrationTest

@IntegrationTest
class TestMultiplesTestInheritance1 extends SuperClassIntegrationSpec {

  def "Should start kafka container"() {
    expect:
    kafkaInstance
    host
  }

}
