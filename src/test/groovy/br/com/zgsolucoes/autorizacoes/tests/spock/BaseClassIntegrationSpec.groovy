package br.com.zgsolucoes.autorizacoes.tests.spock

import br.com.zgsolucoes.autorizacoes.tests.kafka.KafkaConsumerClient
import br.com.zgsolucoes.autorizacoes.tests.kafka.KafkaProducerClient
import br.com.zgsolucoes.autorizacoes.tests.SuperClassIntegrationSpec
import br.com.zgsolucoes.autorizacoes.tests.spock.annotation.Consumer
import br.com.zgsolucoes.autorizacoes.tests.spock.annotation.IntegrationTest
import br.com.zgsolucoes.autorizacoes.tests.spock.annotation.Producer
import org.awaitility.Awaitility
import org.awaitility.Duration
import spock.lang.Shared

@IntegrationTest
class BaseClassIntegrationSpec extends SuperClassIntegrationSpec {

  @Shared
  @Producer
  KafkaProducerClient<String> producer

  @Shared
  @Consumer
  KafkaConsumerClient<String> consumer

  def "Should produce send message"() {
    when:
    producer.send("br.com.zg.teste", '1', 'teste')

    then:
    Awaitility.await().atMost(Duration.FIVE_SECONDS).untilAsserted {
      with(consumer.getSingleRecord(String, 'br.com.zg.teste')) { String test ->
        test == 'teste'
      }
    }
  }
}
