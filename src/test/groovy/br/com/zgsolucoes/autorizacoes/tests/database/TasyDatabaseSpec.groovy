package br.com.zgsolucoes.autorizacoes.tests.database

import br.com.zgsolucoes.autorizacoes.tests.spock.annotation.InjectDatabase
import br.com.zgsolucoes.autorizacoes.tests.spock.annotation.IntegrationTest
import spock.lang.Shared
import spock.lang.Specification

@IntegrationTest
class TasyDatabaseSpec extends Specification {

  @Shared
  @InjectDatabase
  TasyDatabase tasyDatabase

  def "Should start a container running an Oracle Database for TASY"() {
    when:
    int testQuery = tasyDatabase.performQuery("SELECT 1 FROM DUAL").getInt(1)

    then:
    testQuery == 1
  }

}
